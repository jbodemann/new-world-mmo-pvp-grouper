use crate::data_types::{PlayerScorixBot, PlayersScorixBot, ScorixBotEvent};
use crate::Opt;
use core::result::Result::{Err, Ok};
use structopt::StructOpt;

pub fn build_scorix_bot_url(name: String, date: String, time: String) -> String {
    // let base_url = "http://kellerus.de:8096/signup?name=Test%20Siege&date=01.11.2021&time=23:59";
    // let base_url = "https://sun-signup-discordbot.glitch.me";
    let base_url = "https://sun-signup-7d58j.ondigitalocean.app";
    let signup_param = "/signup?name=".to_string();
    let date_param = "&date=".to_string();
    let time_param = "&time=".to_string();
    let clean_name = name.replace(" ", "%20");

    let result = format!(
        "{}{}{}{}{}{}{}",
        base_url, signup_param, clean_name, date_param, date, time_param, time
    );

    // Todo change this to url::url
    // const GITHUB: &'static str = "https://github.com";
    // let base = Url::parse(base_url).expect("hardcoded URL is known to be valid");
    // let joined = base.join(path)?;
    result
}

// reads the sign up players from a google sheet.
pub fn read_signed_up_players_from_scorix_bot(event: ScorixBotEvent) -> Vec<PlayerScorixBot> {
    let mut players: Vec<PlayerScorixBot> = vec![];

    let url = build_scorix_bot_url(event.name, event.date, event.time);
    // let resp = reqwest::blocking::get(url);
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("user-agent", "SUN-new-world-pvp-group-creator")
        .send();

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from kellerus.de: {}", err)
        }
    };
    let result = match response.json::<PlayersScorixBot>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from kellerus: {}", err)
        }
    };

    for p in result.players {
        players.push(p);
    }
    players
}

// reads the sign up players from a google sheet.
pub fn read_event_list_from_scorix_bot() -> Vec<ScorixBotEvent> {
    let mut events: Vec<ScorixBotEvent> = vec![];

    let opt = Opt::from_args();
    let url: String;
    if opt.debug {
        url = "https://sun-signup-7d58j.ondigitalocean.app/events?includeClosed=t".to_string();
    } else {
        url = "https://sun-signup-7d58j.ondigitalocean.app/events".to_string();
    }

    let resp = reqwest::blocking::Client::new()
        .get(&url)
        .header("user-agent", "SUN-new-world-pvp-group-creator")
        .send();

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!(
                "Could not read events from sun-signup-discordbot.glitch.me: {}",
                err
            )
        }
    };
    let result = match response.json::<Vec<ScorixBotEvent>>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json events from kellerus: {}", err)
        }
    };

    for p in result {
        events.push(p);
        // println!("Event: {:?}", p)
    }
    events
}
