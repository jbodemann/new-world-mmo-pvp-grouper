use std::collections::HashSet;
use std::{io, process};

use prettytable::{format, Cell, Table};

use crate::data_types::{InternalRole, RaidGroup, ScorixBotEvent};
use crate::read_from_scorix_bot::read_event_list_from_scorix_bot;
use crate::{player, PlayersType};

extern crate chrono;
use chrono::prelude::*;
use ndarray::Array2;
// use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
use crate::player::name_role_by_id;
use crate::util::format_name;
use crossterm::{
    execute,
    style::{
        Attribute, Color, Print, ResetColor, SetAttribute, SetBackgroundColor, SetForegroundColor,
    },
};
use std::io::stdout;

// #[allow(dead_code)]
// pub(crate) fn print_placed_players_long_version(raid: &Raid, players: &PlayersType) {
//     let mut table = Table::new();
//     table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
//     // table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
//
//     let mut players_print_result = players.clone();
//
//     let role_display_order = [
//         InternalRole::Assassin,
//         InternalRole::Bruiser,
//         InternalRole::CasterAoE,
//         InternalRole::Healer,
//         InternalRole::Paladin,
//         InternalRole::PointTank,
//         InternalRole::RangeAoE,
//         InternalRole::RangeSingle,
//     ];
//     table.set_titles(row![
//         "Type",
//         "Assassin",
//         "Bruiser",
//         "Caster AoE",
//         "Healer",
//         "Paladin",
//         "Point Tank",
//         "Range AoE",
//         "Range Single",
//         "Sum",
//     ]);
//     let mut group_role_used;
//     let mut player_in_group;
//     for g in &raid.groups {
//         player_in_group = 0;
//         let mut row = prettytable::Row::new(vec![]);
//         row.add_cell(Cell::new(&*g.group_type.to_string()));
//         for role in &role_display_order {
//             group_role_used = false;
//             for p in &g.players {
//                 if !group_role_used
//                     && players_print_result.contains_key(p)
//                     && &players_print_result[p].calculated_role == role
//                 {
//                     row.add_cell(Cell::new(p));
//                     group_role_used = true;
//                     player_in_group += 1;
//                     players_print_result.remove(p);
//                 }
//             }
//             if !group_role_used {
//                 row.add_cell(Cell::new(""));
//             }
//         }
//         row.add_cell(Cell::new(&*player_in_group.to_string()));
//         table.add_row(row);
//     }
//     table.printstd();
// }

#[allow(dead_code)]
pub(crate) fn print_placed_players_short_version(raid: &Vec<RaidGroup>, players: &PlayersType) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    // table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

    table.set_titles(row![
        "Group Type",
        "Player 1",
        "Player 2",
        "Player 3",
        "Player 4",
        "Player 5",
    ]);

    let roles_sort_order = [
        InternalRole::Assassin,
        InternalRole::Bruiser,
        InternalRole::CasterAoE,
        InternalRole::Healer,
        InternalRole::Paladin,
        InternalRole::PointTank,
        InternalRole::RangeAoE,
        InternalRole::RangeSingle,
    ];

    for g in raid {
        let mut row = prettytable::Row::new(vec![]);
        row.add_cell(Cell::new(&*g.group_type.to_string()));
        for next_role in &roles_sort_order {
            for p in &g.players {
                if players[p].calculated_role != *next_role {
                    continue;
                }
                row.add_cell(Cell::new(&*format!(
                    "({}) {}",
                    // players[p].calculated_role.to_string().get(0..2).unwrap(),
                    build_role_abbreviation(&players[p].calculated_role),
                    p,
                )));
            }
        }
        for _i in g.players.len()..5 {
            row.add_cell(Cell::new(""));
        }
        table.add_row(row);
    }
    table.printstd();
}

pub(crate) fn print_placed_players_group_vertical(
    raid: &Vec<RaidGroup>,
    players: &PlayersType,
    players_avail: &HashSet<String>,
) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    // table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

    fn write_name_with_role_color(name: &String, role: InternalRole, blink: bool) {
        let mut attr: Attribute = Attribute::NoBlink;
        let mut info_name = name.clone();

        if blink {
            attr = Attribute::Bold;
            info_name = format!("{:-^24}", format!("> {} <", name.trim()));
        }
        let fg;
        let bg;

        match role {
            InternalRole::Assassin => {
                fg = Color::White;
                bg = Color::Magenta;
            }
            InternalRole::Bruiser => {
                fg = Color::White;
                bg = Color::Red;
            }
            InternalRole::CasterAoE => {
                fg = Color::White;
                bg = Color::DarkBlue;
            }
            InternalRole::Healer => {
                fg = Color::White;
                bg = Color::DarkGreen;
            }
            InternalRole::Paladin => {
                fg = Color::White;
                bg = Color::Green;
            }
            InternalRole::PointTank => {
                fg = Color::White;
                bg = Color::DarkYellow;
            }
            InternalRole::RangeAoE => {
                fg = Color::White;
                bg = Color::DarkGrey;
            }
            InternalRole::RangeSingle => {
                fg = Color::Yellow;
                bg = Color::Grey;
            }
            InternalRole::Illegal => {
                fg = Color::White;
                bg = Color::White;
            }
        }
        execute!(
            stdout(),
            SetForegroundColor(fg),
            SetBackgroundColor(bg),
            SetAttribute(attr),
            Print(&info_name),
            ResetColor
        )
        .unwrap();
    }

    let mut a2 = Array2::<usize>::zeros((10, 6));

    for (i, rg) in raid.iter().enumerate() {
        a2[[i, 0]] = i;
        for (j, p) in rg.players.iter().enumerate() {
            a2[[i, j + 1]] = player::id_by_name(p, players);
        }
    }
    let mut player_name: String = ";".to_string();
    let mut blink = false;
    loop {
        print!("\x1B[2J\x1B[1;1H");
        print!("Group 1                 ");
        print!("Group 2                 ");
        print!("Group 3                 ");
        print!("Group 4                 ");
        println!("Group 5                 ");
        for p in 0..6 {
            for g in 0..5 {
                if p == 0 {
                    print!("{}", format_name(raid[a2[[g, p]]].group_type.clone()));
                    continue;
                }
                let name = name_role_by_id(a2[[g, p]], &players).0;
                if name.starts_with(&player_name) {
                    blink = true;
                }
                write_name_with_role_color(
                    &format_name(name_role_by_id(a2[[g, p]], &players).0),
                    name_role_by_id(a2[[g, p]], &players).1,
                    blink,
                );
                blink = false;
            }
            println!();
        }
        println!();
        print!("Group 6                 ");
        print!("Group 7                 ");
        print!("Group 8                 ");
        print!("Group 9                 ");
        println!("Group 10                ");
        for p in 0..6 {
            for g in 5..10 {
                if p == 0 {
                    print!("{}", format_name(raid[a2[[g, p]]].group_type.clone()));
                    continue;
                }
                let name = name_role_by_id(a2[[g, p]], &players).0;
                if name.starts_with(&player_name) {
                    blink = true;
                }
                write_name_with_role_color(
                    &format_name(name_role_by_id(a2[[g, p]], &players).0),
                    name_role_by_id(a2[[g, p]], &players).1,
                    blink,
                );
                blink = false;
            }
            println!();
        }
        print_unplaced_players(&players, &players_avail);

        player_name = read_player_name();
    }
}

pub fn read_player_name() -> String {
    let mut input = String::new();
    println!("Player name: ");
    io::stdin()
        .read_line(&mut input)
        .ok()
        .expect("Couldn't read line");

    let selection = input
        .trim()
        .parse::<String>()
        .expect("You entered something but it was not a string!");
    if selection == "q" {
        process::exit(0);
    }
    selection.clone()
}

#[allow(dead_code)]
fn build_role_abbreviation(role: &InternalRole) -> String {
    match role {
        InternalRole::Assassin => "A".to_string(),
        InternalRole::Bruiser => "B".to_string(),
        InternalRole::CasterAoE => "C".to_string(),
        InternalRole::Healer => "H".to_string(),
        InternalRole::Paladin => "PA".to_string(),
        InternalRole::PointTank => "PT".to_string(),
        InternalRole::RangeAoE => "RA".to_string(),
        InternalRole::RangeSingle => "RS".to_string(),
        InternalRole::Illegal => "I".to_string(),
    }
}
pub(crate) fn print_unplaced_players(players: &PlayersType, player_avial: &HashSet<String>) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);

    let roles = [
        InternalRole::Assassin,
        InternalRole::Bruiser,
        InternalRole::CasterAoE,
        InternalRole::Healer,
        InternalRole::Paladin,
        InternalRole::PointTank,
        InternalRole::RangeAoE,
        InternalRole::RangeSingle,
    ];
    let mut player_by_role = vec![];
    for player_role in roles {
        let mut player_list: Vec<String> = vec![];
        for (name, _role) in players
            .iter()
            .filter(|&(_name, player)| player.calculated_role == player_role)
        {
            if player_avial.contains(name) {
                player_list.push(name.clone());
            }
        }
        player_by_role.push(player_list);
    }

    table.set_titles(row![
        InternalRole::Assassin.to_string(),
        InternalRole::Bruiser.to_string(),
        InternalRole::CasterAoE.to_string(),
        InternalRole::Healer.to_string(),
        InternalRole::Paladin.to_string(),
        InternalRole::PointTank.to_string(),
        InternalRole::RangeAoE.to_string(),
        InternalRole::RangeSingle.to_string(),
        InternalRole::Illegal.to_string(),
    ]);
    let mut printed = true;
    while printed {
        printed = false;
        let mut row = prettytable::Row::new(vec![]);
        for player_with_role in &mut player_by_role {
            if !player_with_role.is_empty() {
                printed = true;
                row.add_cell(Cell::new(&player_with_role.pop().unwrap()));
            } else {
                row.add_cell(Cell::new(""));
            }
        }
        table.add_row(row);
    }
    println!("\nUnplaced players.");
    table.printstd();
}

pub fn ask_user_for_event() -> ScorixBotEvent {
    let events = read_event_list_from_scorix_bot();

    println!("Please select an event by number:");
    for (i, event) in events.iter().enumerate() {
        println!(
            "No: {:2} -> Name: {:?}, Date: {:?} Time: {:?}",
            i, event.name, event.date, event.time
        )
    }
    let mut input = String::new();
    println!("Please enter a number: ");
    io::stdin()
        .read_line(&mut input)
        .ok()
        .expect("Couldn't read line");

    let selection = input
        .trim()
        .parse::<usize>()
        .expect("You entered something but it was not a number!");
    if selection > events.len() - 1 {
        println!("Number out of range. Is it really that difficult?");
        process::exit(0);
    }
    events[selection].clone()
}

pub fn print_players_list(sorted_player_names: &Vec<String>, players: &PlayersType) {
    for name in sorted_player_names {
        let sut = players.get(name).unwrap().signup_time;
        let naive = NaiveDateTime::from_timestamp(sut, 0);
        let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);

        println!(
            "Name: {:18} Gearscore: {:4} Competence {:2} Signup: {} Role: {:24?}",
            players.get(name).unwrap().name,
            players.get(name).unwrap().gear_score,
            players.get(name).unwrap().competence,
            datetime,
            players.get(name).unwrap().calculated_role,
        )
    }
}
