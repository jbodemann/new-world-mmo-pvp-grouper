use crate::data_types::{InternalRole, RaidGroup, ScorixBotEvent};
use crate::read_from_google_sheets::read_predefined_teams_from_google_sheet;
use crate::util::{
    build_player_avail_data_structure, build_players_data_structure,
    build_raid_group_datastructure, get_first_backup_role, get_second_backup_role,
    sort_players_for_raid,
};
use crate::write_results_to_stdout::{
    print_placed_players_group_vertical, print_players_list, print_unplaced_players,
};
use crate::{Opt, PlayersType};
use std::collections::HashSet;
use std::{io, process};
use structopt::StructOpt;
use strum::IntoEnumIterator;

// creates the complete raid, a) add predefined groups b) fill up the open spots
pub fn build_raid(event: ScorixBotEvent) {
    let players = build_players_data_structure(event);
    let mut players_avail = build_player_avail_data_structure(&players);
    let mut raid_groups = build_raid_group_datastructure();

    add_predefined_teams_to_raid(&players, &mut raid_groups, &mut players_avail);
    add_player_to_raid(&players, &mut raid_groups, &mut players_avail);
    // print_placed_players_short_version(&raid_groups, &players);
    print_unplaced_players(&players, &players_avail);
    let mut input = "".to_string();
    println!("Press a key to continue...");
    io::stdin()
        .read_line(&mut input)
        .ok()
        .expect("Couldn't read line");

    print_placed_players_group_vertical(&raid_groups, &players, &players_avail);
}

// adds the predefined teams to the raid
fn add_predefined_teams_to_raid(
    players: &PlayersType,
    raid_groups: &mut Vec<RaidGroup>,
    players_avail: &mut HashSet<String>,
) {
    let predefined_teams = read_predefined_teams_from_google_sheet();

    for team in &predefined_teams {
        if !players_avail.contains(&team.group_leader) && !&team.group_leader.is_empty() {
            println!(
                "WARNING: Group leader '{}' not signed up. Predefined team for '{}' ignored completely.",
                &team.group_leader,
                &team.group_type,
            );
        }
    }
    for team in &predefined_teams {
        // Todo I need to understand the following line
        for raid in &mut *raid_groups {
            if team.group_type == raid.group_type
                && raid.players.is_empty()
                && players_avail.contains(&team.group_leader)
            {
                match team.group_setup.as_ref() {
                    "Backfill" => {
                        add_player_to_specific_group(
                            &team.group_leader,
                            raid,
                            &players,
                            players_avail,
                        );
                        for player in &team.group_members {
                            add_player_to_specific_group(player, raid, players, players_avail)
                        }
                    }
                    "Static" => {
                        force_player_to_specific_group(&team.group_leader, raid, players_avail);
                        for player in &team.group_members {
                            force_player_to_specific_group(player, raid, players_avail)
                        }
                        // Static groups will not be filled up. To achive that we set role_count to zero for all roles
                        set_role_count_to_zero(raid);
                    }
                    _ => {
                        println!("ERROR: Unknown group-setup found: {}", team.group_setup);
                        process::exit(1)
                    }
                }
            }
        }
    }
}

// Static groups will not be filled up, so we set role count to zero
fn set_role_count_to_zero(raid_group: &mut RaidGroup) {
    for role in InternalRole::iter() {
        *raid_group.role_to_count.get_mut(&role).unwrap() = 0;
    }
}

// Add player to a specific group, if the role is still open
fn add_player_to_specific_group(
    player: &str,
    raid_group: &mut RaidGroup,
    players: &PlayersType,
    players_avail: &mut HashSet<String>,
) {
    if players_avail.contains(player) && role_avail(&players[player].calculated_role, &raid_group) {
        players_avail.remove(player);
        raid_group.players.push(player.parse().unwrap());
        *raid_group
            .role_to_count
            .get_mut(&players[player].calculated_role)
            .unwrap() -= 1;
    }
}

pub fn role_avail(role: &InternalRole, raid_group: &RaidGroup) -> bool {
    raid_group.role_to_count[role] > 0
}

// add player to a specific group, ignoring role requirements
fn force_player_to_specific_group(
    player: &str,
    raid_group: &mut RaidGroup,
    players_avail: &mut HashSet<String>,
) {
    // Todo The const is on multiple places, fix it.
    const GROUP_SIZE: usize = 5;

    if raid_group.players.len() < GROUP_SIZE && players_avail.contains(player) {
        players_avail.remove(player);
        raid_group.players.push(player.parse().unwrap());
    }
}

// Add player to next open spot
fn add_player_to_raid(
    players: &PlayersType,
    raid_groups: &mut Vec<RaidGroup>,
    players_avail: &mut HashSet<String>,
) {
    const GROUP_SIZE: usize = 5;

    let sorted_players = sort_players_for_raid(players);
    let opt = Opt::from_args();
    if opt.verbose {
        print_players_list(&sorted_players, players)
    }

    for player in &sorted_players {
        // Todo I need to understand the next line better, BTW the compiler hints are fantastic
        for raid in &mut *raid_groups {
            if players_avail.contains(player)
                && raid.role_to_count[&players[player].calculated_role] > 0
                && raid.players.len() < GROUP_SIZE
            {
                raid.players.push(player.clone());
                *raid
                    .role_to_count
                    .get_mut(&players[player].calculated_role)
                    .unwrap() -= 1;
                players_avail.remove(player);
            }
        }
    }
    for raid in &mut *raid_groups {
        for role in InternalRole::iter() {
            if raid.role_to_count[&role] > 0 {
                for player in &sorted_players {
                    if players_avail.contains(player)
                        && get_first_backup_role(&role) == Some(&players[player].calculated_role)
                        && raid.players.len() < GROUP_SIZE
                    {
                        raid.players.push(player.clone());
                        *raid.role_to_count.get_mut(&role).unwrap() -= 1;
                        players_avail.remove(player);
                        println!(
                            "Player {} with role {:?} placed as backup for {:?}.",
                            &player, &players[player].calculated_role, &role
                        );
                        break;
                    }
                }
            }
        }
    }

    for raid in &mut *raid_groups {
        for role in InternalRole::iter() {
            if raid.role_to_count[&role] > 0 {
                for player in &sorted_players {
                    if players_avail.contains(player)
                        && get_second_backup_role(&role) == Some(&players[player].calculated_role)
                        && raid.players.len() < GROUP_SIZE
                    {
                        raid.players.push(player.clone());
                        *raid.role_to_count.get_mut(&role).unwrap() -= 1;
                        players_avail.remove(player);
                        println!(
                            "Player {} with role {:?} placed as backup 2 for {:?}.",
                            &player, &players[player].calculated_role, &role
                        );
                        break;
                    }
                }
            }
        }
    }
}
