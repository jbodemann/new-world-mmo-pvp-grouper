use std::cmp::Reverse;
use std::collections::{HashMap, HashSet};
use std::process;
// use structopt::StructOpt;

use pickledb::{PickleDb, PickleDbDumpPolicy, SerializationMethod};

use crate::data_types::{
    InternalRole, PlayerScorixBot, RaidGroup, ScorixBotEvent, SignupRole, Weapon,
};
use crate::read_from_google_sheets::{
    read_group_role_definitions_from_google_sheet, read_selected_raid_setup_from_google_sheet,
};
use crate::Player;

use crate::read_from_scorix_bot::read_signed_up_players_from_scorix_bot;
use crate::{read_from_google_sheets, PlayersType};
use strum::IntoEnumIterator; // 0.17.1

#[allow(dead_code)]
pub(crate) fn sort_players_for_raid(players: &PlayersType) -> Vec<String> {
    #[derive(Debug)]
    struct PlayerSorting {
        name: String,
        level: usize,
        gear_score: usize,
        competence: usize,
    }
    // Todo this is most probably not the way to do it. Check the following links:
    // Links:
    // https://users.rust-lang.org/t/idiomatic-oxidised-way-to-store-command-line-arguments-for-later-use/57989/3
    // https://docs.rs/structopt-utilities/0.1.0/structopt_utilities/lazy_static/index.html
    // https://doc.rust-lang.org/core/lazy/struct.Lazy.html
    // let opt = super::Opt::from_args();

    let mut players_for_sorting: Vec<PlayerSorting> = players
        .values()
        .map(|player| PlayerSorting {
            name: player.name.clone(),
            level: player.level,
            gear_score: player.gear_score,
            competence: player.competence,
        })
        .collect();

    // Todo is there a way to remove player.competence for the following statement so we avoid doubling it.
    // if opt.competence {
    //     players_for_sorting
    //         .sort_by_key(|player| Reverse((player.competence, player.level, player.gear_score)));
    // } else {
    //     players_for_sorting.sort_by_key(|player| Reverse((player.level, player.gear_score)));
    // }

    players_for_sorting
        .sort_by_key(|player| Reverse((player.competence, player.level, player.gear_score)));

    // if opt.verbose {
    //     println!("Player sorted by value:");
    //     for p in &players_for_sorting {
    //         println!(
    //             "Competence:{:?} Gear-score:{:?} Level:{:?} Name:{:?}",
    //             p.competence, p.gear_score, p.level, p.name
    //         )
    //     }
    // }
    players_for_sorting.into_iter().map(|p| p.name).collect()
}

// Converts the signup role string to signup role enum
fn string_to_signup_role(role: &str) -> SignupRole {
    match role.to_lowercase().as_ref() {
        "melee dps" => SignupRole::Melee,
        "range singletarget" => SignupRole::Range,
        "range aoe" => SignupRole::Range,
        "range" => SignupRole::Range,
        "healer" => SignupRole::Healer,
        "tank" => SignupRole::Tank,
        _ => {
            panic!("illegal role found: {}", role);
        }
    }
}

// Converts the signup weapon to weapon enum
fn string_to_weapon(weapon: &str) -> Weapon {
    let w = weapon.trim().to_lowercase();

    if w.contains("sword") {
        return Weapon::Sword;
    } else if w.contains("hatchet") {
        return Weapon::Hatchet;
    } else if w.contains("rapier") {
        return Weapon::Rapier;
    } else if w.contains("greataxe") {
        return Weapon::Greataxe;
    } else if w.contains("warhammer") {
        return Weapon::Warhammer;
    } else if w.contains("spear") {
        return Weapon::Spear;
    } else if w.contains("fire") {
        return Weapon::Fire;
    } else if w.contains("ice") {
        return Weapon::Ice;
    } else if w.contains("life") {
        return Weapon::Life;
    } else if w.contains("bow") {
        return Weapon::Bow;
    } else if w.contains("musket") {
        return Weapon::Musket;
    } else if w.contains("void") {
        return Weapon::VoidGauntlet;
    } else {
        panic!("illegal weapon found: {}", weapon);
    }
}

// pub(crate) fn is_spot_open_for_role(group: &RaidGroup, role: &Role) -> bool {
//     // wee need this check since predefined groups don't follow role restrictions
//     if group.role_to_count.values().sum::<usize>() == 5 {
//         return false;
//     }
//     match group.group_type {
//         GroupType::MainLeft | GroupType::MainRight => match role {
//             Role::Initiation => group.role_to_count[&Role::Initiation] == 0,
//             Role::Tank | Role::Melee => {
//                 group.role_to_count[&Role::Tank] + group.role_to_count[&Role::Melee] < 1
//             }
//             Role::Healer => group.role_to_count[&Role::Healer] == 0,
//             Role::Aoe | Role::Range => {
//                 group.role_to_count[&Role::Aoe] + group.role_to_count[&Role::Range] < 2
//             }
//             Role::Shotcaller => false,
//             Role::Illegal => false,
//         },
//         GroupType::FlexLeft | GroupType::FlexRight => match role {
//             Role::Initiation => group.role_to_count[&Role::Initiation] < 2,
//             Role::Tank | Role::Melee => false,
//             Role::Healer => group.role_to_count[&Role::Healer] == 0,
//             Role::Aoe | Role::Range => {
//                 group.role_to_count[&Role::Aoe] + group.role_to_count[&Role::Range] < 2
//             }
//             Role::Shotcaller => false,
//             Role::Illegal => false,
//         },
//         GroupType::Siege => match role {
//             Role::Initiation => false,
//             Role::Tank | Role::Healer | Role::Aoe => {
//                 group.role_to_count[&Role::Tank]
//                     + group.role_to_count[&Role::Healer]
//                     + group.role_to_count[&Role::Aoe]
//                     < 5
//             }
//             Role::Melee => false,
//             Role::Range => false,
//             Role::Shotcaller => false,
//             Role::Illegal => false,
//         },
//         GroupType::SiegeCaller => match role {
//             Role::Initiation => false,
//             Role::Tank | Role::Healer | Role::Aoe => {
//                 group.role_to_count[&Role::Tank]
//                     + group.role_to_count[&Role::Healer]
//                     + group.role_to_count[&Role::Aoe]
//                     < 4
//             }
//             Role::Melee => false,
//             Role::Range => false,
//             Role::Shotcaller => group.role_to_count[&Role::Shotcaller] == 0,
//             Role::Illegal => false,
//         },
//     }
// }

pub(crate) fn build_players_data_structure(event: ScorixBotEvent) -> PlayersType {
    // let opt = super::Opt::from_args();

    // let discord_signup = opt.discord.is_some();
    // #[allow(unused_assignments)] // Todo this seems to be a bug, since players_json is read
    // let mut players_json: Vec<PlayerJson> = Vec::new();
    // let mut discord_json: HashMap<String, PlayerDiscordExport> = HashMap::new();
    // if discord_signup {
    //     players_json = read_complementary_players_data_from_google_sheet();
    //     discord_json = read_discord_bot_export();
    // } else {
    //     players_json = read_from_google_sheets::read_signed_up_players_from_google_sheet();
    // }

    let mut players: PlayersType = HashMap::new();
    let players_json = read_signed_up_players_from_scorix_bot(event);
    let player_competence = read_from_google_sheets::read_player_competence_from_google_sheet();

    for (id, player) in players_json.iter().enumerate() {
        let new_player = Player {
            id: id + 1000, // smallest player id starts with 1000
            name: player.name.clone(),
            signup_role: string_to_signup_role(&player.role),
            calculated_role: player_to_role(player),
            level: player
                .level
                .parse::<usize>()
                .expect(&*format!("level {} is not a number!", player.level)),
            weapon_1: string_to_weapon(&player.weapon_1),
            weapon_2: string_to_weapon(&player.weapon_2),
            gear_score: player.gear_score.parse::<usize>().expect(&*format!(
                "gear score {} is not a number!",
                player.gear_score
            )),
            competence: *player_competence.get(&player.name).unwrap_or(&2),
            signup_time: player.date_of_signup,
        };
        if players.contains_key(&*player.name) {
            println!("player {} found more than once!", player.name);
            process::exit(1);
        }
        players.insert(player.name.clone(), new_player);
    }
    players
}

pub(crate) fn build_player_avail_data_structure(players: &PlayersType) -> HashSet<String> {
    let mut player_avail: HashSet<String> = HashSet::new();
    for player in players.keys() {
        player_avail.insert(player.clone());
    }
    player_avail
}

// pub(crate) fn string_to_group_type(group_type: String) -> GroupType {
//     match group_type.to_lowercase().as_ref() {
//         "juliet" => GroupType::MainLeft,
//         "romeo" => GroupType::MainRight,
//         "flexleft" => GroupType::FlexLeft,
//         "flexright" => GroupType::FlexRight,
//         "siege" => GroupType::Siege,
//         _ => panic!("illegal group type found: {}", group_type),
//     }
// }

fn player_to_role(player: &PlayerScorixBot) -> InternalRole {
    match (
        player.role.as_ref(),
        player.weapon_1.as_ref(),
        player.weapon_2.as_ref(),
    ) {
        ("Melee DPS", "Greataxe", "Spear") => InternalRole::Assassin,
        ("Melee DPS", "Spear", "Greataxe") => InternalRole::Assassin,
        ("Melee DPS", "Greataxe", "Void Gauntlet") => InternalRole::Assassin,
        ("Melee DPS", "Void Gauntlet", "Greataxe") => InternalRole::Assassin,

        ("Melee DPS", "Greataxe", "Warhammer") => InternalRole::Bruiser,
        ("Melee DPS", "Warhammer", "Greataxe") => InternalRole::Bruiser,
        ("Melee DPS", "Greataxe", "Hatchet") => InternalRole::Bruiser,
        ("Melee DPS", "Hatchet", "Greataxe") => InternalRole::Bruiser,

        ("Range AOE", "Ice Gauntlet", "Fire Staff") => InternalRole::CasterAoE,
        ("Range AOE", "Fire Staff", "Ice Gauntlet") => InternalRole::CasterAoE,
        ("Range AOE", "Ice Gauntlet", "Void Gauntlet") => InternalRole::CasterAoE,
        ("Range AOE", "Void Gauntlet", "Ice Gauntlet") => InternalRole::CasterAoE,

        ("Healer", "Ice Gauntlet", "Life Staff") => InternalRole::Healer,
        ("Healer", "Life Staff", "Ice Gauntlet") => InternalRole::Healer,
        ("Healer", "Void Gauntlet", "Life Staff") => InternalRole::Healer,
        ("Healer", "Life Staff", "Void Gauntlet") => InternalRole::Healer,

        ("Healer", "Life Staff", "Warhammer") => InternalRole::Paladin,
        ("Healer", "Warhammer", "Life Staff") => InternalRole::Paladin,

        ("Tank", "Sword and Shield", "Warhammer") => InternalRole::PointTank,
        ("Tank", "Warhammer", "Sword and Shield") => InternalRole::PointTank,

        ("Range AOE", "Bow", _) => InternalRole::RangeAoE,
        ("Range AOE", _, "Bow") => InternalRole::RangeAoE,

        ("Range Singletarget", "Musket", _) => InternalRole::RangeSingle,
        ("Range Singletarget", _, "Musket") => InternalRole::RangeSingle,

        (_, _, _) => {
            println!(
                "WARNING: Illegal role/weapons combination found: Name:{:18} Role:{:20} Weapon:{:12} Weapon:{:12}",
                &player.name, &player.role, &player.weapon_1, &player.weapon_2
            );
            InternalRole::Illegal
            // process::exit(1)
        }
    }
}

pub fn read_api_key() -> String {
    let db = PickleDb::load(
        "key.db",
        PickleDbDumpPolicy::NeverDump,
        SerializationMethod::Json,
    )
    .expect("Could not open key.db {}");
    db.get::<String>("apikey").expect("Could not read api key.")
}

pub fn string_to_internal_role(text: &String) -> InternalRole {
    match text.as_ref() {
        "Assassin" => InternalRole::Assassin,
        "Bruiser" => InternalRole::Bruiser,
        "Caster AOE" => InternalRole::CasterAoE,
        "Healer" => InternalRole::Healer,
        "Paladin" => InternalRole::Paladin,
        "Point Tank" => InternalRole::PointTank,
        "Range AOE" => InternalRole::RangeAoE,
        "Range Single" => InternalRole::RangeSingle,
        _ => {
            panic!("Found illegal role: {}", text)
        }
    }
}

pub fn build_raid_group_datastructure() -> Vec<RaidGroup> {
    let mut raid_groups = Vec::<RaidGroup>::with_capacity(10);
    let selected_raid_setup = read_selected_raid_setup_from_google_sheet();
    let group_role_definitions = read_group_role_definitions_from_google_sheet();

    for (i, group_type) in selected_raid_setup.iter().enumerate() {
        if !group_role_definitions.contains_key(group_type) {
            println!("WARNING: No definition found for group '{}'. Please add the definition to tab 'GroupDefinition' in the google sheet.", group_type);
            process::exit(1);
        }
        let raid_group = RaidGroup {
            group_id: i,
            role_to_count: build_role_to_count(&group_role_definitions[group_type]),
            group_type: group_type.clone(),
            players: vec![],
        };
        raid_groups.push(raid_group)
    }
    raid_groups
}

fn build_role_to_count(group_definition: &Vec<InternalRole>) -> HashMap<InternalRole, usize> {
    let mut role_to_count: HashMap<InternalRole, usize> = HashMap::new();

    // Initialize all roles to 0 to spare error checking later
    for role in InternalRole::iter() {
        role_to_count.insert(role, 0);
    }
    for role in group_definition {
        *role_to_count.get_mut(role).unwrap() += 1;
    }
    role_to_count
}

pub fn format_name(name: String) -> String {
    format!("{}                         ", name)[0..24].to_string()
}

pub fn get_first_backup_role(role: &InternalRole) -> Option<&InternalRole> {
    match role {
        InternalRole::Assassin => Some(&InternalRole::Bruiser),
        InternalRole::Bruiser => Some(&InternalRole::PointTank),
        InternalRole::CasterAoE => None,
        InternalRole::Healer => Some(&InternalRole::Paladin),
        InternalRole::Paladin => Some(&InternalRole::Healer),
        InternalRole::PointTank => Some(&InternalRole::Bruiser),
        InternalRole::RangeAoE => None,
        InternalRole::RangeSingle => None,
        InternalRole::Illegal => None,
    }
}

pub fn get_second_backup_role(role: &InternalRole) -> Option<&InternalRole> {
    match role {
        InternalRole::Assassin => None,
        InternalRole::Bruiser => None,
        InternalRole::CasterAoE => None,
        InternalRole::Healer => None,
        InternalRole::Paladin => None,
        InternalRole::PointTank => Some(&InternalRole::Paladin),
        InternalRole::RangeAoE => None,
        InternalRole::RangeSingle => None,
        InternalRole::Illegal => None,
    }
}
