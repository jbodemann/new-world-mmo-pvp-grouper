use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use strum_macros::EnumIter; // 0.17.1

#[derive(Debug, strum_macros::ToString, PartialEq, Eq, Hash, Clone)]
pub enum SignupRole {
    Healer,
    Melee,
    Range,
    Tank,
}

#[derive(Debug, Clone, strum_macros::ToString)]
pub enum Weapon {
    Sword,
    Hatchet,
    Rapier,
    Greataxe,
    Warhammer,
    Spear,
    Fire,
    Ice,
    Life,
    Bow,
    Musket,
    VoidGauntlet,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PlayerJson {
    pub in_game_name: String,
    pub discord_name: String,
    pub discord_long_id: String,
    pub role: String,
    pub level: String,
    pub weapon_1: String,
    pub weapon_2: String,
    pub gear_score: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct RolesJson {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<PlayerJson>,
}

#[derive(Debug)]
pub struct PredefinedTeams {
    pub group_setup: String,
    pub group_leader: String,
    pub group_type: String,
    pub group_members: Vec<String>,
}

#[derive(Debug)]
pub struct RaidGroup {
    pub group_id: usize,
    pub role_to_count: HashMap<InternalRole, usize>,
    pub group_type: String,
    pub players: Vec<String>,
}
#[allow(dead_code)]
#[derive(Debug)]
pub struct Raid {
    pub groups: Vec<RaidGroup>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PlayerScorixBot {
    pub name: String,
    pub discord_long_id: String,
    pub weapon_1: String,
    pub weapon_2: String,
    pub role: String,
    pub level: String,
    pub gear_score: String,
    pub date_of_signup: i64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PlayersScorixBot {
    pub players: Vec<PlayerScorixBot>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ScorixBotEvent {
    pub name: String,
    pub date: String,
    pub time: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ScorixBotEvents {
    pub events: Vec<ScorixBotEvent>,
}

pub type GroupDefinition = HashMap<String, Vec<InternalRole>>;

#[derive(
    Serialize, Deserialize, Debug, Clone, strum_macros::ToString, EnumIter, PartialEq, Eq, Hash,
)]
pub enum InternalRole {
    Assassin,
    Bruiser,
    CasterAoE,
    Healer,
    Paladin,
    PointTank,
    RangeAoE,
    RangeSingle,
    Illegal,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct GroupDefinitionForSheetRequest {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<GroupDefinitionEntityForSheetRequest>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GroupDefinitionEntityForSheetRequest {
    pub name: String,
    pub role_1: String,
    pub role_2: String,
    pub role_3: String,
    pub role_4: String,
    pub role_5: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct SelectedSetupForSheetRequest {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<SelectedSetupEntityForSheetRequest>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SelectedSetupEntityForSheetRequest {
    pub group: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PredefinedGroupJson {
    pub group_setup: String,
    pub group_leader: String,
    pub group_type: String,
    pub player_1: String,
    pub player_2: String,
    pub player_3: String,
    pub player_4: String,
    pub reserve_1: String,
    pub reserve_2: String,
    pub reserve_3: String,
    pub reserve_4: String,
    pub reserve_5: String,
    pub reserve_6: String,
    pub reserve_7: String,
    pub reserve_8: String,
    pub reserve_9: String,
    pub reserve_10: String,
}
#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct PredefinedGroupRequestResult {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<PredefinedGroupJson>,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct SelectedSetupRequestResult {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct GroupDefinitionsRequestResult {
    pub range: String,
    pub majorDimension: String,
    pub values: Vec<Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct GroupDefinitions {
    pub groups: Vec<GroupDefinition>,
}
