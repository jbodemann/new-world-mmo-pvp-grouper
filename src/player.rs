use crate::data_types::{InternalRole, SignupRole, Weapon};
use crate::PlayersType;

#[derive(Debug, Clone)]
pub struct Player {
    // #[allow(dead_code)] // Todo the warnings with it are wrong
    pub id: usize,
    pub name: String,
    pub signup_role: SignupRole, // player self assigned role (Healer, Melee, Range, Tank)
    pub calculated_role: InternalRole, // this role can also be Initiation, Aoe and Shotcaller, determined by this program
    pub level: usize,
    pub weapon_1: Weapon,
    pub weapon_2: Weapon,
    pub gear_score: usize,
    pub competence: usize, // how well the player handles his char
    pub signup_time: i64,
}

pub fn name_role_by_id(id: usize, players: &PlayersType) -> (String, InternalRole) {
    for p in players.values() {
        if p.id == id {
            return (p.name.clone(), p.calculated_role.clone());
        }
    }
    ("".parse().unwrap(), InternalRole::Illegal)
}

pub fn id_by_name(name: &str, players: &PlayersType) -> usize {
    for p in players.values() {
        if p.name == name {
            return p.id;
        }
    }
    0
}
