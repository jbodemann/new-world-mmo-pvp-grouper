// #![feature(once_cell)]

use crate::player::Player;
use std::collections::HashMap;

// use once_cell::sync::Lazy;
use crate::write_results_to_stdout::ask_user_for_event;
use build_raid::build_raid;
use structopt::StructOpt;

#[macro_use]
extern crate prettytable;

mod build_raid;
mod data_types;
mod player;
mod read_from_google_sheets;
mod read_from_scorix_bot;
mod util;
mod write_result_to_file;
mod write_results_to_stdout;

// build for Windows
// rustup target add x86_64-pc-windows-gnu
// rustup toolchain install stable-x86_64-pc-windows-gnu
// cargo build --release --target x86_64-pc-windows-gnu
// result .exe in /Users/jb/Documents/Projects/new_world_pvp_grouper/target/x86_64-pc-windows-gnu/release/deps

type PlayersType = HashMap<String, Player>;
/// New World PvP Group Builder
#[derive(StructOpt, Debug)]
#[structopt(
    name = "new_world_pvp_grouper",
    about = "Creating a somewhat optimized 50 players setup for 50 v 50 PvP combat.",
    author = "Jörn Bodemann, jb@opv.de",
    version = "0.3"
)]
struct Opt {
    /// Use competence information to calculate player group priority
    #[structopt(short, long)]
    debug: bool,

    /// Print additional information: a) player sorted by group priority (descending)
    #[structopt(short, long)]
    verbose: bool,
}

fn main() {
    let event = ask_user_for_event();
    build_raid(event);
}
