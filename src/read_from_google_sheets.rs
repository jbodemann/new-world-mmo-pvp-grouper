use std::collections::HashMap;
use std::process;

use serde::{Deserialize, Serialize};

use crate::data_types::{
    GroupDefinition, GroupDefinitionEntityForSheetRequest, GroupDefinitionForSheetRequest,
    InternalRole, PredefinedGroupRequestResult, PredefinedTeams, SelectedSetupRequestResult,
};
use crate::util::string_to_internal_role;

pub(crate) fn read_predefined_teams_from_google_sheet() -> Vec<PredefinedTeams> {
    let mut setup_filename = dirs::download_dir().expect("cannot detect path to download dir!");
    setup_filename.push("New-World-Extra - Setup.csv");

    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}",
        sheets_url, sheet_id, "/values/GroupSetup!A2:Q11", api_key
    );
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("User-Agent", "Rust Reqwest")
        .send();
    // .expect("Could not access google sheets api!");

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<PredefinedGroupRequestResult>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };

    let predefined_groups = convert_result_json_to_predefined_group(result);

    predefined_groups
}

fn convert_result_json_to_predefined_group(
    result: PredefinedGroupRequestResult,
) -> Vec<PredefinedTeams> {
    let mut predefined_groups: Vec<PredefinedTeams> = vec![];

    for pgj in result.values {
        let mut pg: PredefinedTeams = PredefinedTeams {
            group_setup: "".to_string(),
            group_leader: "".to_string(),
            group_type: "".to_string(),
            group_members: vec![],
        };

        pg.group_setup = pgj.group_setup;
        pg.group_type = pgj.group_type;
        pg.group_leader = pgj.group_leader;
        pg.group_members.push(pgj.player_1);
        pg.group_members.push(pgj.player_2);
        pg.group_members.push(pgj.player_3);
        pg.group_members.push(pgj.player_4);
        pg.group_members.push(pgj.reserve_1);
        pg.group_members.push(pgj.reserve_2);
        pg.group_members.push(pgj.reserve_3);
        pg.group_members.push(pgj.reserve_4);
        pg.group_members.push(pgj.reserve_5);
        pg.group_members.push(pgj.reserve_6);
        pg.group_members.push(pgj.reserve_7);
        pg.group_members.push(pgj.reserve_8);
        pg.group_members.push(pgj.reserve_9);
        pg.group_members.push(pgj.reserve_10);
        predefined_groups.push(pg);
    }
    predefined_groups
}

pub fn read_player_competence_from_google_sheet() -> HashMap<String, usize> {
    #[derive(Serialize, Deserialize, Debug)]
    struct PlayerJson {
        name: String,
        skill: String,
    }
    #[derive(Serialize, Deserialize, Debug)]
    #[allow(non_snake_case)]
    struct RolesJson {
        range: String,
        majorDimension: String,
        values: Vec<PlayerJson>,
    }
    let mut player_competence_filename =
        dirs::download_dir().expect("cannot detect path to download dir!");
    player_competence_filename.push("New-World-Extra - Skills.csv");

    let mut player_name_to_competence: HashMap<String, usize> = HashMap::new();
    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}",
        sheets_url, sheet_id, "/values/Skills!A2:B200", api_key
    );

    // let resp = reqwest::blocking::get(url);
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("User-Agent", "Rust Reqwest")
        .send();
    // ;        .expect("Could not access google sheets api!");

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<RolesJson>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };

    for player in result.values {
        let player_name = player.name;
        if player_name.is_empty() {
            continue;
        }
        let player_skill = player.skill.parse::<usize>().expect(&*format!(
            "Skill of player {} is not a number!",
            &player_name
        ));

        if player_name_to_competence.contains_key(&*player_name) {
            println!(
                "player {} found more than once in competence list!",
                &player_name,
            );
            process::exit(1);
        }
        player_name_to_competence.insert(player_name, player_skill);
    }
    player_name_to_competence
}

pub(crate) fn read_group_role_definitions_from_google_sheet() -> GroupDefinition {
    // let mut group_definitions: Vec<GroupDefinition> = vec![];
    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}{}{}",
        sheets_url, sheet_id, "/values/", "GroupDefinitions", "!A2:F200", api_key
    );

    let resp = reqwest::blocking::get(url);
    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from GroupDefinitions tab: {}", err)
        }
    };
    let result = match response.json::<GroupDefinitionForSheetRequest>() {
        Ok(result) => result,
        Err(err) => {
            panic!(
                "Could not read json data from GroupDefinitions tab: {}",
                err
            )
        }
    };

    // for gd in result.values {
    //     group_definitions.push(gd);
    // }
    let group_definitions = sheet_group_definition_to_internal_group_definition(&result.values);
    // println!("{:?}", &group_definitions);
    group_definitions
}

fn sheet_group_definition_to_internal_group_definition(
    group_definitions: &Vec<GroupDefinitionEntityForSheetRequest>,
) -> GroupDefinition {
    let mut internal_group_definition: GroupDefinition = HashMap::new();
    for gd in group_definitions {
        let mut roles: Vec<InternalRole> = vec![];
        roles.push(string_to_internal_role(&gd.role_1));
        roles.push(string_to_internal_role(&gd.role_2));
        roles.push(string_to_internal_role(&gd.role_3));
        roles.push(string_to_internal_role(&gd.role_4));
        roles.push(string_to_internal_role(&gd.role_5));
        internal_group_definition.insert(gd.name.clone(), roles);
    }
    internal_group_definition
}

pub(crate) fn read_selected_raid_setup_from_google_sheet() -> Vec<String> {
    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}",
        sheets_url, sheet_id, "/values/GroupSetup!A15:A24", api_key
    );

    // let resp = reqwest::blocking::get(url);
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("User-Agent", "Rust Reqwest")
        .send();
    // ;        .expect("Could not access google sheets api!");

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<SelectedSetupRequestResult>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };
    let mut group_setup = vec![];
    for gs in &result.values {
        group_setup.push(gs.first().unwrap().clone())
    }
    group_setup
}
